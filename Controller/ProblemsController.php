<?
class ProblemsController extends AppController {
  public $helpers = array('Html', 'Form','Js');
  public $components = array('Session','RequestHandler');

  public function index(){
    $this->set('title_for_layout', 'Home');
  }
  public function testing(){
    $this->set('title_for_layout', 'Testing');
  }
  public function getProblem(){
    if ($this->RequestHandler->isAjax()){
      $this->autoRender = false;
      $maxId = $this->Problem->find('MaxId');
      $minId = $this->Problem->find('MinId');
      $maxId = $maxId[0][0]['max'];
      $minId = $minId[0][0]['min'];

      $randomRow = rand($minId,$maxId);
      $problem = $this->Problem->find('getProblem',array('conditions'=>array('pid'=>$randomRow)));
      return json_encode($problem[0]['Problem']);
    }    
  }
  public function checkAnswer(){
    if ($this->RequestHandler->isAjax()){
      $this->autoRender = false;
      $pid = $this->request->data['pid'];
      $answer = $this->request->data['answer'];
      $problem = $this->Problem->find('first',array('fields'=>'problem','conditions'=>array('pid'=>$pid)));
      $result = $this->evalExpression($problem['Problem']['problem']);

      if($result == $answer){
        $check = "Correct";
      }else {
        $check = "Wrong";
      }
      $this->loadModel('Statistic');
      $this->Statistic->insertStat($pid,0,$check);
      return json_encode(array('result'=>$check));
    }
  }
  protected function evalExpression($prob){
    $f = create_function('','return (int)(' . $prob . ');');
    return $f();
  }
}
