<?php
class StatisticsController extends AppController {
  public function statistics(){
    $this->set('title_for_layout', 'Statistics');
    $this->set('total',$this->Statistic->find('count'));
    $this->set('correct',$this->Statistic->find('correct'));
    $this->set('wrong',$this->Statistic->find('wrong'));
  }
}
