<?php
App::uses('Sanitize','Utility');
class Statistic extends AppModel {
  public $findMethods = array('correct'=>true,'wrong'=>true);

  protected function _findCorrect($state,$query,$results = array()){
    if ($state == 'before'){
      $query['fields'] = 'count(Statistic.aid) as correct';
      $query['conditions']['Statistic.answered'] = 'Correct';
      return $query;
    }
    return $results;
  }    
  protected function _findWrong($state,$query,$results = array()){
    if ($state == 'before'){
      $query['fields'] = 'count(Statistic.aid) as wrong';
      $query['conditions']['Statistic.answered'] = 'Wrong';
      return $query;
    }
    return $results;
  }
  public function insertStat($pid,$uid,$check){
    $pid = Sanitize::escape($pid);
    $uid = Sanitize::escape($uid);
    $check = Sanitize::escape($check);
    $this->query("insert into statistics values('',$pid,$uid,'$check')");
  }
}
