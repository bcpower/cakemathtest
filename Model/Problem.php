<?php
class Problem extends AppModel{
  public $findMethods = array('MaxId'=>true,'MinId'=>true,'getProblem'=>true);

  protected function _findMaxId($state,$query,$results = array() ){
    if($state == 'before'){
      $query['fields']= 'max(pid) as max';
      return $query;
    }
    return $results;
  }
  protected function _findMinId($state,$query,$results = array() ){
    if($state == 'before'){
      $query['fields']= 'min(pid) as min';
      return $query;
    }
    return $results;
  }
  protected function _findGetProblem($state,$query,$results = array() ){
    if($state == 'before'){
      $query['fields']= array('pid','problem');
      return $query;
    }
    return $results;
  }
}
