<?php echo $this->Html->script('jquery.min',false); ?>
<?php echo $this->Html->script('testing',false); ?>
<div id="pageTitle">
  Testing Center
</div>
<div id="artical">
  Welcome to the Testing Center. Here you can test you math ablity.<br/>
  Questions will be on the left and your results will be shown on the right.
  <br/>
  <br/>
</div>
<div id="mathTest">
  <div id="problem">
    <div id="current">
      <div id='question'>
      </div>
        <br/>
        <br/>
      <div id="answers">
        <br/>
        <input type="text" name="answer" id="answer"/>
        <input type="button" value="Answer" onclick="updateQuestion()"/>
      </div>
    </div>
    <div id="pastP">
      Past Questions
      <table id='past'>
      </table>
    </div>
  </div>
</div>
<?php echo $this->Js->writeBuffer(array('cache'=>true)); ?>
