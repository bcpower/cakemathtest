<div id='menu'>
  <ul>
    <li>
      <?php echo $this->Html->link('Home',
        array('controller'=>'problems', 'action'=>'index')); ?>
    </li>
    <li>
      <?php echo $this->Html->link('Testing',
        array('controller'=>'problems', 'action'=>'testing')); ?>
    </li>
    <li>
      <?php echo $this->Html->link('Statistics',          
        array('controller'=>'statistics', 'action'=>'statistics')); ?>
    </li>
  </ul>
</div>
