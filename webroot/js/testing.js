$(document).ready(function(){
  displayProblem();

  $('#answer').keydown(function(evt){
    if ((evt.keyCode) && (evt.keyCode == 13)){
      updateQuestion();
    }
  });

  $('#answer').focus();
});

function displayProblem(){
  var problem = getProblem();
  var problemHtml = "";

  problemHtml  = "<input id='problemID' type='hidden' value='"+problem.pid+"'/>";
  problemHtml += "<div id='problemDisplay'>"+problem.problem+"</div>";
  $('#question').html(problemHtml);
}
function getProblem(){
  var problem = jQuery.parseJSON(
      $.ajax({
        url:'getProblem',
        type:'POST',
        dataType:'json',
        async:false
      }).responseText
  );
  return problem;
}

function updateQuestion(){
  var data = getAnswer();
  var answer = data[2];
  var result = checkAnswer(answer);
  var html = "<tr><td>"+data[1]+" = "+data[0]+"</td><td>"+result+"</td></tr>";

  $('#past').append(html);
  if($('#past tr').length > 16){
    $('#past tr:first').remove();
  }
  $('#answer').val('');
  displayProblem();
}
function checkAnswer(answer){
  var answerCheck = jQuery.parseJSON(
      $.ajax({
        url:"checkAnswer",
        type:"Post",
        data:answer,
        dataType:"json",
        async:false
      }).responseText
    );
  return answerCheck.result;
}
function getAnswer(){
  var pid = $('#problemID').val();
  var answer = $('#answer').val();
  var problem = $('#problemDisplay').text();
  var data = "pid="+pid+"&answer="+answer;
  var result = Array();

  result.push(answer);
  result.push(problem);
  result.push(data);

  return result;
}
